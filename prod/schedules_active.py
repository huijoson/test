from prod.gitlab_api import GitlabApi


class SchedulesAcitve:

    @staticmethod
    def set_multi_schedules_active():
        gitlab_api = GitlabApi()
        gitlab_api.set_schedules_if_active(True)

    @staticmethod
    def set_multi_schedules_inactive():
        gitlab_api = GitlabApi()
        gitlab_api.set_schedules_if_active(False)

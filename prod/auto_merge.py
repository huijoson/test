import json
import requests


class AutoMerge:

    def __init__(self):
        self.gitlab_header = {'private-token': 'D5WNyqdVJKuaydDyDoDk'}
        self.base_url = 'http://10.2.145.70'
        self.api_root = '/api/v4'
        self.group_id = 43

    def get_opened_merge_request(self):
        opened_info_list = []
        url = f"{self.base_url}{self.api_root}/groups/{self.group_id}/merge_requests?state=opened&scope=assigned_to_me"

        response = requests.get(url=url,
                                headers=self.gitlab_header)

        if response.status_code != 200:
            print(response.content)

        else:
            opened_list = json.loads(response.content)

            for request in opened_list:
                iid = request['iid']
                project_id = request['project_id']
                merge_info = {
                    'iid': iid,
                    'project_id': project_id
                }
                opened_info_list.append(merge_info)

        return opened_info_list

    def update_opened_to_merged(self):
        opened_info_list = self.get_opened_merge_request()
        response_list = []

        for merge_obj in opened_info_list:
            project_id = merge_obj['project_id']
            iid = merge_obj['iid']
            url = f"{self.base_url}{self.api_root}/projects/{project_id}/merge_requests/{iid}/merge"

            response = requests.put(url=url,
                                    headers=self.gitlab_header)
            response_list.append(json.loads(response.content))

        return response_list

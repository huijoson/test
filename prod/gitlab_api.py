import json

import paramiko
import requests


class GitlabApi:
    def __init__(self) -> None:
        super().__init__()
        self.__gitlab_header = {'private-token': 'D5WNyqdVJKuaydDyDoDk'}
        self.__base_url = 'http://10.2.145.70'
        self.__api_root = '/api/v4'
        self.__group_id = 43
        self.projects_id = []
        self.data = []
        self.intt_staging_hosts = ['10.2.105.89', '10.2.105.85', '10.2.145.49', '10.2.105.178', '10.2.105.86']

    @property
    def get_gitlab_header(self):
        return self.__gitlab_header

    @property
    def get_prefix_url(self):
        return self.__base_url + self.__api_root

    def get_projects_id(self):
        full_url = f"{self.__base_url}{self.__api_root}/groups/{self.__group_id}/search"
        self.response = requests.get(full_url, headers=self.__gitlab_header,
                                     params={'scope': 'projects', 'search': '', 'page': 1})

        if self.response.status_code != 200:
            print(self.response.content)
        else:
            self.data += self.response.json()
            while self.response.headers['X-Next-Page']:
                self.response = requests.get(url=full_url,
                                             params={'scope': 'projects', 'search': '',
                                                     'page': self.response.headers['X-Next-Page']},
                                             headers=self.__gitlab_header)

                if self.response.status_code != 200:
                    print('get projects error: ' + str(self.response.content))
                self.data += self.response.json()

        for project in self.data:
            self.projects_id.append(project["id"])

        return self.projects_id

    def set_schedules_if_active(self, active):
        project_dict = {
            # pur
            216: 38,
            # mdm
            918: 46,
            # tp
            761: 37
        }
        url_list = [f"{self.__base_url}{self.__api_root}/projects/{u}/pipeline_schedules/{project_dict[u]}" \
                    for u in project_dict]
        flag = True
        for url in url_list:
            response = requests.put(url=url, data={'active': active},
                                    headers=self.__gitlab_header)
            if response.status_code != 200:
                flag = False
            else:
                result = json.loads(response.content)
                print(result)
        return flag

    def upgrade_all_modules(self):
        for host in self.intt_staging_hosts:
            trans = paramiko.Transport((host, 22))
            trans.connect(username="odoo", password="h6ru4HRPMiNiTeAmru84e.4yj3")

            ssh = paramiko.SSHClient()
            ssh._transport = trans

            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)

            command = "sh ./odooKill.sh && ./odooTest.sh all && ./odooStart.sh && ./odooCheck.sh"
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)

            print(ssh_stdout.readlines())

            ssh.close()

    def get_ap_logs(self):
        hosts = {
            'MDM': '10.2.145.81',
            'PUR': '10.2.145.85',
            'TP': '10.2.105.101',
            'HL': '10.2.109.166',
            'GS': '10.2.109.231',
            'DL': '10.2.109.249',
            'TC': '10.2.109.101',
            'UL': '10.2.109.241',
        }

        for company in hosts:
            trans = paramiko.Transport((hosts[company], 22))
            trans.connect(username="odoo", password="h6ru4HRPMiNiTeAmru84e.4yj3")

            ssh = paramiko.SSHClient()
            ssh._transport = trans

            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)

            command = "sudo find /var/log/ -name *messages*"
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)

            # replace \n
            formatted_list = [item.replace("\n", "") for item in ssh_stdout.readlines() if item]
            print(formatted_list)

            ftp_client = ssh.open_sftp()

            for item in formatted_list:
                ftp_client.get(f'{item}', f'E:\\messages\\{company}\\{item.replace("/var/log/","")}')

            ssh.close()


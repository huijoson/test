from auto_merge import AutoMerge
from new_branches import NewBranches
from prod.get_ap_logs import GetApLogsModules
from prod.schedules_active import SchedulesAcitve
from prod.upgrade_all_modules import UpgradeAllModules


def main():
    option = input("Choice what you want?\n 1. Auto Merge...\n 2. New Branches\n 3. Active/Inactive Schedules...\n 4. "
                   "Upgrade All modules for Intt and Staging...\n 5. Get ap logs...\n ")

    # To auto merge requests
    if option == '1':
        auto_merge = AutoMerge()
        result = auto_merge.update_opened_to_merged()
        print(f"{result} \U0001F923")

    # To new branches
    elif option == '2':
        NewBranches.new_branches()

    # To Active/Inactive Schedules
    elif option == '3':
        sub_option = input("Choice what you want?\n 1. To Active Schedules...\n 2. To Inactive Schedules...\n ")
        if sub_option == '1':
            SchedulesAcitve.set_multi_schedules_active()
        elif sub_option == '2':
            SchedulesAcitve.set_multi_schedules_inactive()

    # To Upgrade All Modules
    elif option == '4':
        UpgradeAllModules.upgrade_modules()

    # To Get Ap Logs
    elif option == '5':
        GetApLogsModules.get_ap_logs()


if __name__ == "__main__":
    main()

import json

import requests

from prod.gitlab_api import GitlabApi


class NewBranches:

    @staticmethod
    def new_branches():
        gitlab_api = GitlabApi()
        project_ids = gitlab_api.get_projects_id()
        prefix_url = gitlab_api.get_prefix_url()
        gitlab_header = gitlab_api.get_gitlab_header()

        for project_id in project_ids:
            full_url = f"{prefix_url}/projects/{project_id}/repository/branches?branch=pre-staging&ref=staging"
            response = requests.post(full_url, headers=gitlab_header)
            json_data = json.loads(response.text)
            print(json_data)

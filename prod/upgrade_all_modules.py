from prod.gitlab_api import GitlabApi


class UpgradeAllModules:

    @staticmethod
    def upgrade_modules():
        gitlab_api = GitlabApi()
        gitlab_api.upgrade_all_modules()

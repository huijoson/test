import os

from flask import Flask, send_from_directory, jsonify
from flask_restful import Api, Resource, reqparse
from api.HelloApiHandler import HelloApiHandler
from flask_cors import CORS

# @app.route('/')
# def hello_world():
#     return 'Hello World!'
#
#
# @app.route('/get_list')
# def get_project_list():
#     pass
#
#
# @app.route('/index')
# def show_index():
#     Agent()
#     name = "yuhan"
#     return render_template('index.html', name=name)

#
# if __name__ == '__main__':
#     app.run()
from prod.auto_merge import AutoMerge

app = Flask(__name__, static_folder='frontend/public')
CORS(app)


# Serve React App
@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def serve(path):
    if path != "" and os.path.exists(app.static_folder + '/' + path):
        return send_from_directory(app.static_folder, path)
    else:
        return send_from_directory(app.static_folder, 'index.html')


@app.route('/movies')
def movies():
    return jsonify(dd=[
        {'rating': 3, 'title': 'harry potter'},
        {'rating': 4, 'title': 'harry potter2'}
    ])


@app.route('/auto_merge')
def auto_merge():
    auto_merge_obj = AutoMerge()
    result = auto_merge_obj.update_opened_to_merged()
    print(result)


if __name__ == '__main__':
    app.run(use_reloader=True, port=5000, threaded=True)


import json
import unittest
import requests

gitlab_header = {'private-token': 'D5WNyqdVJKuaydDyDoDk'}
base_url = 'http://10.2.145.70'
api_root = '/api/v4'
project_dict = {
    # pur
    216: 38,
    # mdm
    918: 46,
    # tp
    761: 37
}


def get_single_active_project_id():
    url_list = [f"{base_url}{api_root}/projects/{u}/pipeline_schedules/{project_dict[u]}" \
                for u in project_dict]
    print(url_list)
    for url in url_list:
        response = requests.get(url=url,
                                headers=gitlab_header)
        result = json.loads(response.content)
        print(result)


def set_schedules_if_active(active):
    url_list = [f"{base_url}{api_root}/projects/{u}/pipeline_schedules/{project_dict[u]}" \
                for u in project_dict]
    flag = True
    for url in url_list:
        response = requests.put(url=url, data={'active': active},
                                headers=gitlab_header)
        if response.status_code != 200:
            flag = False
        else:
            result = json.loads(response.content)
            print(result)
    return flag


class PipelineScheduleTestCase(unittest.TestCase):
    def test_get_single_project_id(self):
        url_list = [f"{base_url}{api_root}/projects/{u}/pipeline_schedules/{project_dict[u]}" \
                    for u in project_dict]
        print(url_list)
        for url in url_list:
            response = requests.get(url=url,
                                    headers=gitlab_header)
            result = json.loads(response.content)
            print(result)
        self.assertEqual(True, True)

    def test_set_multi_schedules_active(self):
        self.assertTrue(True, set_schedules_if_active(True))

    def test_set_multi_schedules_inactive(self):
        self.assertEqual(True, set_schedules_if_active(False))


if __name__ == '__main__':
    unittest.main()

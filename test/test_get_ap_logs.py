import unittest

import paramiko

hosts = {
         # 'MDM': '10.2.145.81',
         'PUR': '10.2.145.85',
         # 'TP': '10.2.105.101',
         # 'HL': '10.2.109.166',
         # 'GS': '10.2.109.231',
         # 'DL': '10.2.109.249',
         # 'TC': '10.2.109.101',
         # 'UL': '10.2.109.241',
         }


class GetApLogsTestCase(unittest.TestCase):
    def test_do_ssh_command(self):
        for company in hosts:
            trans = paramiko.Transport((hosts[company], 22))
            trans.connect(username="odoo", password="h6ru4HRPMiNiTeAmru84e.4yj3")

            ssh = paramiko.SSHClient()
            ssh._transport = trans

            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)

            command = "sudo find /var/log/ -name *messages*"
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)

            # replace \n
            formatted_list = [item.replace("\n", "") for item in ssh_stdout.readlines() if item]
            print(formatted_list)

            ftp_client = ssh.open_sftp()

            for item in formatted_list:
                print(company)
                ftp_client.get(f'{item}', f'E:\\messages\\{company}\\{item.replace("/var/log/","")}')

            ssh.close()

        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()

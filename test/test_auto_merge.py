import json
import unittest
import requests

gitlab_header = {'private-token': 'D5WNyqdVJKuaydDyDoDk'}
base_url = 'http://10.2.145.70'
api_root = '/api/v4'
group_id = 43


def get_opened_merge_request():
    opened_info_list = []
    url = f"{base_url}{api_root}/groups/{group_id}/merge_requests?state=opened"

    response = requests.get(url=url,
                            headers=gitlab_header)
    opened_list = json.loads(response.content)

    for request in opened_list:
        iid = request['iid']
        project_id = request['project_id']
        merge_info = {
            'iid': iid,
            'project_id': project_id
        }
        opened_info_list.append(merge_info)

    return opened_info_list


class MyTestCase(unittest.TestCase):
    def test_get_opened_merge_request(self):
        opened_info_list = []
        url = f"{base_url}{api_root}/groups/{group_id}/merge_requests?state=opened&scope=assigned_to_me"

        response = requests.get(url=url,
                                headers=gitlab_header)
        opened_list = json.loads(response.content)
        print(opened_list)
        for request in opened_list:
            iid = request['iid']
            project_id = request['project_id']
            merge_info = {
                'iid': iid,
                'project_id': project_id
            }
            opened_info_list.append(merge_info)
        print(opened_info_list)

        self.assertEqual(True, True)

    def test_update_opened_to_merged(self):
        opened_info_list = get_opened_merge_request()

        for merge_obj in opened_info_list:
            project_id = merge_obj['project_id']
            iid = merge_obj['iid']
            url = f"{base_url}{api_root}/projects/{project_id}/merge_requests/{iid}/merge"

            response = requests.put(url=url,
                                    headers=gitlab_header)
            result = json.loads(response.content)
            print(result)

            self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()

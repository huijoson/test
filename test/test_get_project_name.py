import unittest

import requests

gitlab_header = {'private-token': 'D5WNyqdVJKuaydDyDoDk'}
base_url = 'http://10.2.145.70'
api_root = '/api/v4'
project_id = 43


class MyTestCase(unittest.TestCase):
    def test_something(self):
        temp_dict = {}
        data = []
        projects_id = []
        full_url = f"{base_url}{api_root}/groups/{project_id}/search"
        response = requests.get(full_url, headers=gitlab_header,
                                params={'scope': 'projects', 'search': '', 'page': 1})

        if response.status_code != 200:
            print(response.content)
        else:
            data += response.json()
            while response.headers['X-Next-Page']:
                response = requests.get(url=full_url,
                                        params={'scope': 'projects', 'search': '',
                                                'page': response.headers['X-Next-Page']},
                                        headers=gitlab_header)
                print(response.headers)

                if response.status_code != 200:
                    print('get projects error: ' + str(response.content))
                data += response.json()

        for project in data:
            projects_id.append(project["id"])

        print(projects_id)

        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()

import json
import unittest

import requests

gitlab_header = {'private-token': 'D5WNyqdVJKuaydDyDoDk'}
base_url = 'http://10.2.145.70'
api_root = '/api/v4'


class MyTestCase(unittest.TestCase):
    def test_search_one_commit_merged(self):
        uri = f"{base_url}{api_root}/merge_requests?state=merged&assignee_id=57"
        print(uri)
        response = requests.get(uri, headers=gitlab_header)
        json_data = json.loads(response.text)
        print(json_data)
        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()

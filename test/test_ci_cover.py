import subprocess
import unittest
import git
import paramiko
import shutil
import os

from git import Repo
from git.refs import reference

hosts = ['10.2.105.101', '10.2.145.81', '10.2.145.85']


def get_hosts_modules_list(ip, mod_list):
    trans = paramiko.Transport((ip, 22))
    trans.connect(username="odoo", password="h6ru4HRPMiNiTeAmru84e.4yj3")

    ssh = paramiko.SSHClient()
    ssh._transport = trans

    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)

    command = "cd /opt/odoo/openerp/addons_pohai && ls -d */"
    ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)
    dirs_list = ssh_stdout.readlines()
    for dirs in dirs_list:
        new_director = dirs.replace("/\n", "")
        if new_director not in ['eip', 'kpi', 'password_security_enhance', 'report_qweb_pdf_watermark',
                                'smile_redis_session_store', 'snippet_latest_posts', 'utility', 'web_froala_editor',
                                'web_lib', 'web_progressbar', 'web_responsive_list_view', 'website_news',
                                'website_snippet_dynamic', 'web_tree_field_selection', 'web_x2m_multi_extra_fill']:
            mod_list.append(new_director)
    print(mod_list)

    ssh.close()

    return mod_list


def git(*args):
    return subprocess.check_call(['git'] + list(args))


class MyTestCase(unittest.TestCase):
    def test_get_host_modules_list(self):
        arrange_tp_list = []
        arrange_mdm_list = []

        arrange_tp_list = get_hosts_modules_list('10.2.105.101', arrange_tp_list)
        arrange_mdm_list = get_hosts_modules_list('10.2.145.81', arrange_mdm_list)

        print(arrange_tp_list)

        new_mdm_list = [elem for elem in arrange_mdm_list if elem not in arrange_tp_list]
        print(new_mdm_list)

        for prj in arrange_tp_list:
            Repo.clone_from(f"http://10.2.145.70/tzuchi_odoo_grp/{prj}.git", f"E:\\PythonProject\\test\src\\ap\\{prj}")

        # for prj in new_mdm_list:
        #     Repo.clone_from(f"http://10.2.145.70/tzuchi_odoo_grp/{prj}.git", f"E:\\PythonProject\\test\src\\mdm\\{prj}")

        # for prj in arrange_tp_list:
        #         git("status")
        #         git("clone", f"git@10.2.145.70:tzuchi_odoo_grp/{prj}.git")

        self.assertEqual(True, True)

    def test_ssh_to_runner(self):
        trans = paramiko.Transport(('10.2.145.60', 22))
        trans.connect(username="odoo", password="h6ru4HRPMiNiTeAmru84e.4yj3")

        ssh = paramiko.SSHClient()
        ssh._transport = trans

        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)
        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("ls -d */")
        print(ssh_stdout.read())

        ssh.close()

        self.assertEqual(True, True)

    def test_copy_ci(self):
        arrange_tp_list = []
        arrange_mdm_list = []

        arrange_tp_list = get_hosts_modules_list('10.2.105.101', arrange_tp_list)
        # arrange_mdm_list = get_hosts_modules_list('10.2.145.81', arrange_mdm_list)

        print(arrange_tp_list)

        # new_mdm_list = [elem for elem in arrange_mdm_list if elem not in arrange_tp_list]
        # print(new_mdm_list)

        paths = [f'E:\\PythonProject\\test\src\\ap\\{prj}\\' for prj in arrange_tp_list]
        print(paths)
        for path in paths:
            print(path)
            shutil.copy(r"E:\PythonProject\test\src\ap\.gitlab-ci.yml", path)

        self.assertEqual(True, True)

    def test_git_commit_and_push(self):
        arrange_tp_list = []
        arrange_tp_list = get_hosts_modules_list('10.2.105.101', arrange_tp_list)

        print(arrange_tp_list)

        paths = [f'E:\\PythonProject\\test\src\\ap\\{prj}\\' for prj in arrange_tp_list]
        print(paths)
        for path in paths:
            print(path)
            repo = Repo(path=path)
            repo.git.checkout('develop')
            # repo.git.reset('--hard', 'origin/master')
            repo.git.add(A=True)
            repo.index.commit('CI manual intt')
            repo.git.push(refspec="develop:origin")


        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()

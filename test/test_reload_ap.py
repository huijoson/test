import unittest

import paramiko

hosts = ['10.2.105.101', '10.2.105.102']


class ReloadApTestCase(unittest.TestCase):
    def test_do_ssh_command(self):
        for host in hosts:
            trans = paramiko.Transport((host, 22))
            trans.connect(username="odoo", password="h6ru4HRPMiNiTeAmru84e.4yj3")

            ssh = paramiko.SSHClient()
            ssh._transport = trans

            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)

            command = "sh ./odooKill.sh && ./odooUpdate.sh hra &&./odooTest.sh hra && ./odooStartup.sh && ./odooCheck.sh"
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)

            # ftp_client = ssh.open_sftp()
            # ftp_client.get('')
            ssh_stdout.readlines()
            print(ssh_stdout.readlines())

            ssh.close()

        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()

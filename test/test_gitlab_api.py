import json
import unittest
import requests

gitlab_header = {'private-token': 'D5WNyqdVJKuaydDyDoDk'}
base_url = 'http://10.2.145.70'
api_root = '/api/v4'
project_id = 43


class GitLabApi:
    def __init__(self):
        self.projects_id = []
        self.data = []

    def get_projects_id(self):
        full_url = f"{base_url}{api_root}/groups/{project_id}/search"
        self.response = requests.get(full_url, headers=gitlab_header,
                                     params={'scope': 'projects', 'search': '', 'page': 1})

        if self.response.status_code != 200:
            print(self.response.content)
        else:
            self.data += self.response.json()
            while self.response.headers['X-Next-Page']:
                self.response = requests.get(url=full_url,
                                             params={'scope': 'projects', 'search': '',
                                                     'page': self.response.headers['X-Next-Page']},
                                             headers=gitlab_header)

                if self.response.status_code != 200:
                    print('get projects error: ' + str(self.response.content))
                self.data += self.response.json()

        for project in self.data:
            self.projects_id.append(project["id"])

        return self.projects_id


class GitlabTestCase(unittest.TestCase):

    def setUp(self) -> None:
        super().setUp()

    def test_get_projects_list(self):
        self.data = []
        self.projects_id = []
        self.response = object

        full_url = f"{base_url}{api_root}/groups/{project_id}/search"
        self.response = requests.get(full_url, headers=gitlab_header,
                                     params={'scope': 'projects', 'search': '', 'page': 1})

        if self.response.status_code != 200:
            print(self.response.content)
        else:
            self.data += self.response.json()
            while self.response.headers['X-Next-Page']:
                self.response = requests.get(url=full_url,
                                             params={'scope': 'projects', 'search': '',
                                                     'page': self.response.headers['X-Next-Page']},
                                             headers=gitlab_header)

                if self.response.status_code != 200:
                    print('get projects error: ' + str(self.response.content))
                self.data += self.response.json()

        for project in self.data:
            self.projects_id.append(project["id"])

        print(self.projects_id)

        self.assertEqual((True if 295 in self.projects_id else False), True)

    def test_add_projects_protection_for_staging(self):
        env_list = ['develop', 'qat', 'uat', 'intt', 'staging']
        url = f"{base_url}{api_root}/projects/903/protected_branches?name=develop"

        print(url)
        response = requests.delete(url=url,
                                   headers=gitlab_header)
        print(response.content)

        for env in env_list:

            if env == 'dev' or env == 'qat' or env == 'uat' or env == 'intt':
                url = f"{base_url}{api_root}/projects/903/protected_branches?name={env}&push_access_level=30" \
                      f"&merge_access_level=30"
            else:
                url = f"{base_url}{api_root}/projects/903/protected_branches?name={env}&push_access_level=40" \
                      f"&merge_access_level=40"
            result = requests.post(url=url,
                                   params={'scope': 'projects', 'search': ''},
                                   headers=gitlab_header)
        print(result.content)
        self.assertEqual(True, True)

    def test_delete_projects_protection_for_staging(self):
        project_ids = GitLabApi().get_projects_id()

        for temp_id in project_ids:
            url = '{base_url}/{api_root}/{path}'.format(base_url='http://10.2.145.70',
                                                        api_root='api/v4',
                                                        path=f'projects/{temp_id}/protected_branches/' + 'staging')
            response = requests.delete(url=url, headers=gitlab_header)
            print(response)
        self.assertEqual(True, True)

    def test_change_project_protection_for_every_staging(self):
        project_ids = GitLabApi().get_projects_id()

        for p_id in project_ids:
            # delete protection
            url = '{base_url}/{api_root}/{path}'.format(base_url='http://10.2.145.70',
                                                        api_root='api/v4',
                                                        path=f'projects/{p_id}/protected_branches/' + 'staging')
            response = requests.delete(url=url, headers=gitlab_header)
            print(response)

            # update protection
            url = f"{base_url}{api_root}/projects/{p_id}/protected_branches?name=staging&push_access_level=40" \
                  f"&merge_access_level=40"

            print(url)

            result = requests.post(url=url,
                                   params={'scope': 'projects', 'search': ''},
                                   headers=gitlab_header)
            print(result.content)

        self.assertEqual(True, True)


def get_project_name():
    temp_dict = {}
    full_url = f"{base_url}{api_root}/groups/{project_id}"
    response = requests.get(full_url, headers=gitlab_header,
                            params={'scope': 'projects', 'search': '', 'page': 1})

    if response.status_code != 200:
        print(response.content)
    else:
        data += response.json()
        while self.response.headers['X-Next-Page']:
            self.response = requests.get(url=full_url,
                                         params={'scope': 'projects', 'search': '',
                                                 'page': self.response.headers['X-Next-Page']},
                                         headers=gitlab_header)

            if self.response.status_code != 200:
                print('get projects error: ' + str(self.response.content))
            self.data += self.response.json()

    for project in self.data:
        self.projects_id.append(project["id"])

    print(self.projects_id)

    self.assertEqual((True if 295 in self.projects_id else False), True)


if __name__ == '__main__':
    unittest.main()

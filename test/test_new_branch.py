import json
import unittest
import requests

gitlab_header = {'private-token': 'D5WNyqdVJKuaydDyDoDk'}
base_url = 'http://10.2.145.70'
api_root = '/api/v4'
group_id = 43


class GitLabApi:
    def __init__(self):
        self.projects_id = []
        self.data = []

    def get_projects_id(self):
        full_url = f"{base_url}{api_root}/groups/{group_id}/search"
        self.response = requests.get(full_url, headers=gitlab_header,
                                     params={'scope': 'projects', 'search': '', 'page': 1})

        if self.response.status_code != 200:
            print(self.response.content)
        else:
            self.data += self.response.json()
            while self.response.headers['X-Next-Page']:
                self.response = requests.get(url=full_url,
                                             params={'scope': 'projects', 'search': '',
                                                     'page': self.response.headers['X-Next-Page']},
                                             headers=gitlab_header)

                if self.response.status_code != 200:
                    print('get projects error: ' + str(self.response.content))
                self.data += self.response.json()

        for project in self.data:
            self.projects_id.append(project["id"])

        return self.projects_id


class MyTestNewBranchCase(unittest.TestCase):
    def test_get_project_ids(self):
        project_ids = GitLabApi().get_projects_id()
        print(project_ids)
        self.assertEqual(True, True)

    def test_new_a_branch(self):
        project_ids = GitLabApi().get_projects_id()

        for project_id in project_ids:
            full_url = f"{base_url}{api_root}/projects/{project_id}/repository/branches?branch=pre-staging&ref=staging"
            response = requests.post(full_url, headers=gitlab_header)
            json_data = json.loads(response.text)
            print(json_data)
        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()

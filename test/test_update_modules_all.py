import unittest

import paramiko

hosts = ['10.2.105.89', '10.2.105.85', '10.2.145.49', '10.2.105.178', '10.2.105.86']


class ModulesUpdateTestCase(unittest.TestCase):
    def test_do_ssh_command(self):
        for host in hosts:
            trans = paramiko.Transport((host, 22))
            trans.connect(username="odoo", password="h6ru4HRPMiNiTeAmru84e.4yj3")

            ssh = paramiko.SSHClient()
            ssh._transport = trans

            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)

            command = "sh ./odooKill.sh && ./odooTest.sh all && ./odooStart.sh && ./odooCheck.sh"
            ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command(command)

            print(ssh_stdout.readlines())

            ssh.close()

        self.assertEqual(True, True)


if __name__ == '__main__':
    unittest.main()
